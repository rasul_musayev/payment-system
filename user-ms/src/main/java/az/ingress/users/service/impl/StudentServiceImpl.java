package az.ingress.users.service.impl;

import az.ingress.users.dto.StudentDto;
import az.ingress.users.entity.Course;
import az.ingress.users.entity.Student;
import az.ingress.users.exception.CourseNotFoundException;
import az.ingress.users.exception.StudentNotFoundException;
import az.ingress.users.repostitory.CourseRepository;
import az.ingress.users.repostitory.StudentRepository;
import az.ingress.users.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    private final ModelMapper modelMapper;

    private final CourseRepository courseRepository;

    @Override
    public Page<StudentDto> getAllStudents(Pageable pageable) {
        return studentRepository.findAll(pageable).
                map(students -> modelMapper.map(students, StudentDto.class));
    }

    @Override
    public StudentDto getStudentById(Long id) {
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new StudentNotFoundException("Student not found with ID: " + id));
        return modelMapper.map(student, StudentDto.class);
    }

    @Override
    public StudentDto createStudent(StudentDto studentDto) {

        Student student = modelMapper.map(studentDto, Student.class);
        studentRepository.save(student);
        return modelMapper.map(student, StudentDto.class);
    }

    @Override
    public StudentDto updateStudent(Long id, StudentDto studentDto) {
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new StudentNotFoundException("Student not found with ID: " + id));

        modelMapper.map(studentDto, student);

        Student updatedStudent = studentRepository.save(student);
        return modelMapper.map(updatedStudent, StudentDto.class);
    }

    @Override
    public void deleteStudent(Long id) {
        Student student = studentRepository.findById(id)
                .orElseThrow(() -> new StudentNotFoundException("Student not found with ID: " + id));
        studentRepository.delete(student);
    }

    @Override
    public void removeCourseFromStudent(Long studentId, Long courseId) {
        Course course = courseRepository.findById(courseId)
                .orElseThrow(() -> new CourseNotFoundException("Course  not found with ID: " + courseId));

        Student student = studentRepository.findById(studentId)
                .orElseThrow(() -> new StudentNotFoundException("Student  not found with ID : " + studentId));

        course.getStudents().remove(student);
        courseRepository.save(course);
    }



        @Override
        public void enrollStudentI0nCourseByNumber(Long studentId, String courseNumber) {
            Student student = studentRepository.findById(studentId)
                    .orElseThrow(() -> new StudentNotFoundException("Student not found with ID: " + studentId));

            Course course = courseRepository.findByCourseNumber(courseNumber);
            if (course == null) {
                throw new CourseNotFoundException("Course not found with course number: " + courseNumber);
            }

            enrollStudentInCourse(student, course);
    }


//    @Override
//    public List<Student> getStudentsByName(String name) {
//        return studentRepository.findByNameContainingIgnoreCase(name);
//
//    }
//
//    @Override
//    public List<Student> getStudentsByEmail(String email) {
//        return studentRepository.findByEmail(email);
//
//    }
//
//    @Override
//    public List<Student> getStudentsByNumber(String studentNumber) {
//        return studentRepository.findByStudentNumber(studentNumber);
//
//    }

    private void enrollStudentInCourse(Student student, Course course) {
        if (!course.getStudents().contains(student)) {
            course.getStudents().add(student);
            courseRepository.save(course);
        }
    }
}

