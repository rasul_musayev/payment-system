package az.ingress.users.controller;

import az.ingress.users.dto.StudentDto;
import az.ingress.users.entity.Student;
import az.ingress.users.service.CourseService;
import az.ingress.users.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {
    private final StudentService studentService;
    private final CourseService courseService;

    @PostMapping("/create/student")
    @ResponseStatus(HttpStatus.CREATED)
    public StudentDto createStudent(@Valid @RequestBody StudentDto studentDto) {
        log.info("Received request {}", studentDto);
        return studentService.createStudent(studentDto);
    }

    @PutMapping("/update/{id}")
    public StudentDto updateUser(@PathVariable Long id, @RequestBody StudentDto studentDto) {
        log.info("Received request {}", studentDto);
        return studentService.updateStudent(id, studentDto);
    }

    @GetMapping("/students/{id}")
    public StudentDto getStudentById(@PathVariable Long id) {
        return studentService.getStudentById(id);
    }

    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable Long id) {
        studentService.deleteStudent(id);
    }

    @GetMapping("/all-students")
    public Page<StudentDto> listUsers(Pageable pageable) {
        return studentService.getAllStudents(pageable);
    }






//    @DeleteMapping("/removeCourseFromStudent")
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    public void removeCourseFromStudent(@RequestParam Long studentId, @RequestParam Long courseId) {
//        studentService.removeCourseFromStudent(studentId, courseId);
//    }
//
//    @PostMapping("/enrollStudent")
//    public void enrollStudentInCourseByNumber(@RequestParam Long studentId, @RequestParam String courseNumber) {
//        studentService.enrollStudentInCourseByNumber(studentId, courseNumber);
//    }
//
//    @GetMapping("/studentsNames")
//    public List<Student> getStudentsByName(@RequestParam String name) {
//        return studentService.getStudentsByName(name);
//    }
//
//    @GetMapping("/studentsEmail")
//    public List<Student> getStudentsByEmail(@RequestParam String email) {
//        return studentService.getStudentsByEmail(email);
//    }
//
//    @GetMapping("/studentsNumber")
//    public List<Student> getStudentsByNumber(@RequestParam String studentNumber) {
//        return studentService.getStudentsByNumber(studentNumber);
//    }

}
