package az.ingress.users.dto;

import az.ingress.users.entity.RoleEnum;
import az.ingress.users.entity.Student;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CourseDto {

     private String course;

     @Enumerated(EnumType.STRING)
     RoleEnum isActiveRole;

     private Student student;

}
