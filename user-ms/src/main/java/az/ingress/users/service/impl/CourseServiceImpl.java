package az.ingress.users.service.impl;

import az.ingress.users.dto.CourseDto;
import az.ingress.users.dto.CourseSearchRequestDto;
import az.ingress.users.entity.Course;
import az.ingress.users.entity.CourseStatus;
import az.ingress.users.entity.Student;
import az.ingress.users.exception.CourseNotFoundException;
import az.ingress.users.exception.StudentNotFoundException;
import az.ingress.users.repostitory.CourseRepository;
import az.ingress.users.repostitory.StudentRepository;
import az.ingress.users.service.CourseService;
import az.ingress.users.specification.CourseSpecifications;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class    CourseServiceImpl implements CourseService {

    private final CourseRepository courseRepository;
    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;


    @Override
    public Page<CourseDto> getAllCourses(Pageable pageable) {
        return courseRepository.findAll(pageable).
                map(course -> modelMapper.map(course, CourseDto.class));
    }

    @Override
    public CourseDto createCourse(CourseDto courseDto) {
        Course course = modelMapper.map(courseDto, Course.class);
        courseRepository.save(course);
        return modelMapper.map(course, CourseDto.class);
    }

    @Override
    public CourseDto updateCourse(Long id, CourseDto courseDto) {
        Course course = courseRepository.findById(id)
                .orElseThrow(() -> new CourseNotFoundException("Course not found with id: " + id));

        modelMapper.map(courseDto, course);

        Course updatedCourse = courseRepository.save(course);
        return modelMapper.map(updatedCourse, CourseDto.class);
    }

    @Override
    public void deleteCourse(Long id) {
        Course course = courseRepository.findById(id)
                .orElseThrow(() -> new CourseNotFoundException("Course not found with id: " + id));
        courseRepository.delete(course);
    }

    @Override
    public Page<CourseDto> searchCourses(CourseSearchRequestDto searchRequestDto, Pageable pageable) {
        Specification<Course> spec = Specification.where(null);

        if (searchRequestDto.getName() != null && !searchRequestDto.getName().isEmpty()) {
            spec = spec.and(CourseSpecifications.filterByName(searchRequestDto.getName()));
        }

        if (searchRequestDto.getCourse() != null && !searchRequestDto.getCourse().isEmpty()) {
            spec = spec.and(CourseSpecifications.filterByCourse(searchRequestDto.getCourse()));
        }

        spec = spec.and(CourseSpecifications.filterByIsActive(searchRequestDto.isActive()));

        if (searchRequestDto.getStudent() != null) {
            spec = spec.and(CourseSpecifications.filterByStudent(searchRequestDto.getStudent()));
        }
        Page<Course> coursePage = courseRepository.findAll(spec, pageable);
        return coursePage.map(this::convertToDto);
    }

    @Override
    public List<Student> getStudentsInCourse(Long courseId) {
        Optional<Course> courseOptional = courseRepository.findById(courseId);
        if (courseOptional.isPresent()) {
            Course course = courseOptional.get();
            return new ArrayList<>(course.getStudents());
        } else {
            return Collections.emptyList();
        }
    }
//
//    @Override
//    public void addStudentToCourse(Long studentId, Long courseId) {
//        Student student = studentRepository.findById(studentId)
//                .orElseThrow(() -> new StudentNotFoundException("Student not found with ID: " + studentId));
//
//        Course course = courseRepository.findById(courseId)
//                .orElseThrow(() -> new CourseNotFoundException("Course not found with ID: " + courseId));
//
//        student.getCourses().add(course);
//        course.getStudents().add(student);
//
//        studentRepository.save(student);
//    }
//
//    @Override
//    public void removeStudentFromCourse(Long studentId, Long courseId) {
//        Course course = courseRepository.findById(courseId)
//                .orElseThrow(() -> new IllegalArgumentException("Course not found with ID  : " + courseId));
//
//        course.getStudents().removeIf(student -> student.getId().equals(studentId));
//        courseRepository.save(course);
//    }
//
//    @Override
//    public List<Course> getCoursesByName(String courseName) {
//        return courseRepository.findByCourseNameContainingIgnoreCase(courseName);
//
//    }
//
//    @Override
//    public void updateCourseStatus(Long courseId, CourseStatus newStatus) {
//        courseRepository.findById(courseId)
//                .ifPresent(course -> {
//                    course.setStatus(newStatus);
//                    courseRepository.save(course);
//                });
//    }

    private CourseDto convertToDto(Course course) {
        return modelMapper.map(course, CourseDto.class);
    }

    private static Student fecthStudent(Student studentId) {
        return Optional.ofNullable(studentId)
                .orElseThrow(() -> new StudentNotFoundException("Student not found with ID: " + studentId));
    }
}