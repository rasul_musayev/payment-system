package az.ingress.users.controller;

import az.ingress.users.dto.CourseDto;
import az.ingress.users.dto.CourseStudentRequestDto;
import az.ingress.users.entity.Course;
import az.ingress.users.entity.CourseStatus;
import az.ingress.users.service.CourseService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/courses")
@RequiredArgsConstructor
public class CourseController {

    private final CourseService courseService;

    @PostMapping("/create/course")
    @ResponseStatus(HttpStatus.CREATED)
    public CourseDto createCourse(@Valid @RequestBody CourseDto courseDto) {
        log.info("Received request {}", courseDto);
        return courseService.createCourse(courseDto);
    }

    @PutMapping("/update/{id}")
    public CourseDto updateCourse(@PathVariable Long id, @RequestBody CourseDto courseDto) {
        return courseService.updateCourse(id, courseDto);
    }

    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable Long id) {
        courseService.deleteCourse(id);
    }

    @GetMapping("/all-courses")
    public Page<CourseDto> listUsers(Pageable pageable) {
        return courseService.getAllCourses(pageable);
    }






//
//    @PostMapping("/addStudentToCourse")
//    public void addStudentToCourse(@RequestBody CourseStudentRequestDto requestDto) {
//        courseService.addStudentToCourse(requestDto.getStudentId(), requestDto.getCourseId());
//    }
//
//
//    @ResponseStatus(HttpStatus.NO_CONTENT)
//    @DeleteMapping("/removeStudentFromCourse")
//    public void removeStudentFromCourse(@RequestParam Long studentId, @RequestParam Long courseId) {
//        courseService.removeStudentFromCourse(studentId, courseId);
//    }
//
//    @GetMapping("/getCoursesByName")
//    public List<Course> getCoursesByName(@RequestParam String courseName) {
//        return courseService.getCoursesByName(courseName);
//    }
//
//    @PutMapping("/courses/{courseId}/status")
//    @ResponseStatus(HttpStatus.OK)
//    public void updateCourseStatus(@PathVariable Long courseId, @RequestParam CourseStatus newStatus) {
//        courseService.updateCourseStatus(courseId, newStatus);
//    }

}
