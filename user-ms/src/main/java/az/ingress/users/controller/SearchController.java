package az.ingress.users.controller;

import az.ingress.users.dto.CourseDto;
import az.ingress.users.dto.CourseSearchRequestDto;
import az.ingress.users.service.CourseService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/search/api")
@RequiredArgsConstructor
public class SearchController {

    private final CourseService courseService;

    @GetMapping("/course/")
    public ResponseEntity<Page<CourseDto>> searchBooks(@RequestBody CourseSearchRequestDto request, Pageable pageable) {
        return ResponseEntity.ok(courseService.searchCourses(request, pageable));
    }

}
