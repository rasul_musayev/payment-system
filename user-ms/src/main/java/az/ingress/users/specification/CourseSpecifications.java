package az.ingress.users.specification;

import az.ingress.users.entity.Course;
import az.ingress.users.entity.Student;
import org.springframework.data.jpa.domain.Specification;

public class CourseSpecifications {
    public static Specification<Course> filterByName(String name) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("name"), name);
    }

    public static Specification<Course> filterByCourse(String course) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("course"), course);
    }

    public static Specification<Course> filterByIsActive(boolean isActive) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("isActive"), isActive);
    }

    public static Specification<Course> filterByStudent(Student student) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("student"), student);
    }

}

