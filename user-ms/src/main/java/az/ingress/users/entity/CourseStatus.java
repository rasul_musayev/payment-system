package az.ingress.users.entity;

public enum CourseStatus {
    OPEN,
    CLOSED,
    CANCELLED
}
