package az.ingress.users.repostitory;

import az.ingress.users.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student,Long> {
//    List<Student> findByNameContainingIgnoreCase(String name);
//
//    List<Student> findByEmail(String email);
//
//    List<Student> findByStudentNumber(String studentNumber);
}
