
package az.ingress.users.repostitory;

import az.ingress.users.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface CourseRepository extends JpaRepository<Course, Long>, JpaSpecificationExecutor<Course> {


//    List<Course> findByCourseNameContainingIgnoreCase(String courseName);
    Course findByCourseNumber(String courseNumber);


}
