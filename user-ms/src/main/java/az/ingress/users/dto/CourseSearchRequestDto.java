package az.ingress.users.dto;

import az.ingress.users.entity.Student;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CourseSearchRequestDto {

    private String name;
    private String course;
    private boolean isActive;
    private Student student;

}
