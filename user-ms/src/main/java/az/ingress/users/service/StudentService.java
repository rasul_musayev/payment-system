package az.ingress.users.service;

import az.ingress.users.dto.StudentDto;
import az.ingress.users.entity.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StudentService {
    Page<StudentDto> getAllStudents(Pageable pageable);

    StudentDto getStudentById(Long id);

    StudentDto createStudent(StudentDto studentDto);

    StudentDto updateStudent(Long id, StudentDto studentDto);

    void deleteStudent(Long id);


    void removeCourseFromStudent(Long studentId, Long courseId);


    void enrollStudentI0nCourseByNumber(Long studentId, String courseNumber);


//    List<Student> getStudentsByName(String name);
//    List<Student> getStudentsByEmail(String email);
//    List<Student> getStudentsByNumber(String studentNumber);


}
