package az.ingress.users.service;

import az.ingress.users.dto.CourseDto;
import az.ingress.users.dto.CourseSearchRequestDto;
import az.ingress.users.entity.Course;
import az.ingress.users.entity.CourseStatus;
import az.ingress.users.entity.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CourseService {

    Page<CourseDto> getAllCourses(Pageable pageable);

    CourseDto createCourse(CourseDto courseDTO);

    CourseDto updateCourse(Long id, CourseDto courseDTO);

    void deleteCourse(Long id);

    Page<CourseDto> searchCourses(CourseSearchRequestDto searchRequestDto, Pageable pageable);



    List<Student> getStudentsInCourse(Long courseId);



//    void addStudentToCourse(Long studentId, Long courseId);
//    void removeStudentFromCourse(Long studentId, Long courseId);
//
//    List<Course> getCoursesByName(String courseName);
//    void updateCourseStatus(Long courseId, CourseStatus newStatus);
//




}
